class Welford
{
	public:
		Welford( uint64_t limit=0xFFFFFFFFFFFFFFFF ) : mMean( 0.0 ), mM2( 0.0 ), mMin( 0.0 ), mMax( 0.0 ), mCount( 0 ), mLimit( limit ) { }
		void     Reset( void ) { mMean = 0.0; mM2 = 0.0; mMin = 0.0; mMax = 0.0; mCount = 0; }
		uint64_t SetLimit( uint64_t limit ) { return mLimit = limit; }
		uint64_t Limit( void ) const { return mLimit; }
		uint64_t Count( void ) const { return mCount; }
		bool     Finished( void ) const { return mCount >= mLimit; }
		double   Mean( void ) const { return mCount ? mMean : sqrt( -1.0 ); }
		double   Min( void ) const { return mCount ? mMin : sqrt( -1.0 ); }
		double   Max( void ) const { return mCount ? mMax : sqrt( -1.0 ); }
		double   StandardDeviation( uint64_t sampleBiasCorrection=1 ) const { return ( mCount > sampleBiasCorrection ) ? sqrt( mM2 / ( mCount - sampleBiasCorrection ) ) : sqrt( -1.0 ); }
		double   Process( double x )
		{
			if( !mCount )  mMin = mMax = x;
			if( x < mMin ) mMin = x;
			if( x > mMax ) mMax = x;
			double delta = x - mMean;
			mMean += delta / ++mCount;
			mM2 += delta * ( x - mMean );
			return x;
		}
	private:
		double   mMean;
		double   mM2;
		double   mMin;
		double   mMax;
		uint64_t mCount;
		uint64_t mLimit;
};
