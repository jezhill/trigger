#ifndef __TriggerProcessing_H__
#define __TriggerProcessing_H__
#include <Arduino.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////

class Trigger
{
  public:

    Trigger( int channelID, int inputPin, int outputPin );
    
    void ProcessSample( void );
    void UpdateThreshold( void );
    bool ProcessOutputCycle( bool combine_with );
    void Report( void );

    int  OutputPin( void ) { return mOutputPin; }
    void SetThresholding( int thresholdPin, float thresholdScaling ) { mThresholdPin = thresholdPin; mThresholdScaling = thresholdScaling; }
    void SetTiming( float minTriggerDurationMsec, float minTriggerPeriodMsec, float samplingRateHz );

  private:

    int mChannelID;
    int mInputPin;
    int mThresholdPin;
    int mOutputPin;
    unsigned long mMinTriggerDurationSamples;
    unsigned long mMinTriggerPeriodSamples;
    
    float mSignal;
    float mThreshold;
    float mThresholdScaling;
    bool mTriggerOn;

    unsigned long mSamplesSinceTrigger;
    float mPreviousReading;
    float mRecentMaxAbsDiff;
    
};

int msec2samples( float msec, float samplingRateHz )
{
   return ( int )( 0.5 + msec * samplingRateHz / 1000.0 );
}

float samples2msec( float samples, float samplingRateHz )
{
  return 1000.0 * samples / samplingRateHz;
}

Trigger::Trigger( int channelID, int inputPin, int outputPin )
{
  
  mChannelID = channelID;
  mInputPin = inputPin;
  mThresholdPin = -1;
  mOutputPin = outputPin;
  mThresholdScaling = 1000.0;
  mMinTriggerDurationSamples = 100;
  mMinTriggerPeriodSamples = 0;

  if( mInputPin >= 0 )     pinMode( mInputPin,     INPUT );
  if( mThresholdPin >= 0 ) pinMode( mThresholdPin, INPUT );
  if( mOutputPin >= 0 )    pinMode( mOutputPin,    OUTPUT );
  if( mOutputPin >= 0 )    digitalWrite( mOutputPin, LOW );
    
  mTriggerOn = false;
  mSamplesSinceTrigger = 0;
  
  mPreviousReading = 0.0;
  mRecentMaxAbsDiff = 0.0;

}  
void Trigger::SetTiming( float minTriggerDurationMsec, float minTriggerPeriodMsec, float samplingRateHz )
{
  mMinTriggerDurationSamples = msec2samples( minTriggerDurationMsec, samplingRateHz );
  mMinTriggerPeriodSamples   = msec2samples( minTriggerPeriodMsec,   samplingRateHz );
}


// Modification to Trigger::ProcessSample which uses numerical difference
// as used in the audiomath-gitrepo (PlayerLatency-TeensySketch.ino)
void Trigger::ProcessSample( void ) // TODO: Add refractory period
{
  
  bool triggerWasOn = mTriggerOn;
  if( mInputPin >= 0 )
  {
    mSignal = ( float )analogRead( mInputPin ); // where mSignal is a correlate for reading
    float absDiff = fabs( mSignal - mPreviousReading ); //initially mPreviousReading should be 0
    mPreviousReading = mSignal;
    
    if ( absDiff > mThreshold)
    { 
      // Threshold is defined by the threshold pin
      if( mSamplesSinceTrigger == 0 || mSamplesSinceTrigger >= mMinTriggerPeriodSamples ) mTriggerOn = true;
    }
    mRecentMaxAbsDiff = max( mRecentMaxAbsDiff, absDiff ); //regularly get the most recent maximum of an absDiff value

  }
  if( mTriggerOn && !triggerWasOn ) mSamplesSinceTrigger = 0;
  if( mTriggerOn || mSamplesSinceTrigger ) mSamplesSinceTrigger++;  
  if( mSamplesSinceTrigger > mMinTriggerDurationSamples ) mTriggerOn = false;
}

void Trigger::UpdateThreshold( void )
{
    if( mThresholdPin >= 0 ) mThreshold = ( float )analogRead( mThresholdPin );
    else mThreshold = 65535.0;
    mThreshold *= mThresholdScaling / 65535.0; // read potentiometer
}

bool Trigger::ProcessOutputCycle( bool combine_with )
{
    UpdateThreshold();
        
    bool triggerHigh = mTriggerOn | combine_with;
    if( mOutputPin >= 0 ) digitalWrite( mOutputPin, triggerHigh ? HIGH : LOW );
    return triggerHigh;
}

void Trigger::Report( void )
{
  Serial.print( mChannelID ); Serial.print( ": {" );
  Serial.print( "'signal':" );        Serial.print( mSignal, 10 );       Serial.print( ", " );
  Serial.print( "'max_abs_diff': " ); Serial.print( mRecentMaxAbsDiff ); Serial.print( ", " );
  Serial.print( "'threshold':" );     Serial.print( mThreshold, 10 );    Serial.print( ", " );
  Serial.print( "'trigger':" );       Serial.print( mTriggerOn );        //Serial.print( ", " );
  Serial.print( "}, " );
  mRecentMaxAbsDiff = 0.0;
}
#endif // __TriggerProcessing_H__
