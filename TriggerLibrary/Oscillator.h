#ifndef __Oscillator_H__
#define __Oscillator_H__
#include "Arduino.h"

class Oscillator;
int NUMBER_OF_OSCILLATORS = 0;
const int MAX_NUMBER_OF_OSCILLATORS = 4;
Oscillator * OSCILLATOR_POINTERS[ MAX_NUMBER_OF_OSCILLATORS ] = { NULL, NULL, NULL, NULL };
typedef void ( *OSCILLATOR_CALLBACK )();
extern const OSCILLATOR_CALLBACK OSCILLATOR_CALLBACKS[]; 

class Oscillator : public IntervalTimer
{
	public:
		Oscillator( double oscillationFrequencyHz, double samplingFrequencyHz )
		{
			mSamplesPerSecond = samplingFrequencyHz;
			mCyclesPerSecond = oscillationFrequencyHz;
			mSampleReady = false;
			mSampleCounter = 0;
			int myIndex = NUMBER_OF_OSCILLATORS++;
			if( myIndex < MAX_NUMBER_OF_OSCILLATORS )
			{
				OSCILLATOR_POINTERS[ myIndex ] = this;
				begin( OSCILLATOR_CALLBACKS[ myIndex ], 1e6 / mSamplesPerSecond );
			}
		}
		~Oscillator() {}
		double CyclesPerSecond( void ) const { return mCyclesPerSecond; }
		double Seconds( void ) const { return mSampleCounter / mSamplesPerSecond; }
		double Radians( void ) const { return 2.0 * PI * mCyclesPerSecond * ( mSampleCounter / mSamplesPerSecond ); }
		uint32_t Samples( void ) const { return mSampleCounter; }
		void   SetCyclesPerSecond( double freq_hz ) { mCyclesPerSecond = freq_hz; mSampleCounter = 0; }
		bool   Tick( void ) { bool result = mSampleReady; if( result ) mSampleReady = false; return result; }
		void   SampleCallback()
		{
			++mSampleCounter;
			if( !mSampleCounter && mCyclesPerSecond )
			{
				// At, say, 10 kHz sampling rate, wraparound of a 32-bit sample counter
				// happens every 5 days of continuous operation and causes a phase
				// glitch. This minimizes the glitch by setting mSampleCounter such
				// that the phase of a wave of frequency mCyclesPerSecond is the same
				// (to within half a sample) as it would be at at mSampleCounter=2**32
				double samplesPerCycle = mSamplesPerSecond / mCyclesPerSecond;
				mSampleCounter = ( uint32_t )( 0.5 + fmod( 4294967296.0, samplesPerCycle ) );
			}
			mSampleReady = true;
		}
	private:
		volatile bool     mSampleReady;
		volatile uint32_t mSampleCounter;
		double            mSamplesPerSecond;
		double            mCyclesPerSecond;
};
#define DEFINE_OSCILLATOR_CALLBACK( N )  void OscillatorCallback ## N ( void ) { if( OSCILLATOR_POINTERS[ N ] ) OSCILLATOR_POINTERS[ N ]->SampleCallback(); }
DEFINE_OSCILLATOR_CALLBACK( 0 )
DEFINE_OSCILLATOR_CALLBACK( 1 )
DEFINE_OSCILLATOR_CALLBACK( 2 )
DEFINE_OSCILLATOR_CALLBACK( 3 )
const OSCILLATOR_CALLBACK OSCILLATOR_CALLBACKS[ MAX_NUMBER_OF_OSCILLATORS ] = { OscillatorCallback0, OscillatorCallback1, OscillatorCallback2, OscillatorCallback3 };

#endif // __Oscillator_H__
