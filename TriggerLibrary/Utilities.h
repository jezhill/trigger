#ifndef __Utilities_H__
#define __Utilities_H__
#include "Arduino.h"

#include "SerialInterface.h"

#include "TriggerBoxV2.h"
void
Hello( int msecPerLED, int nRepeats )
{
	for( int repeat = 0; repeat < nRepeats; repeat++ )
	{
		for( int outputBit = 0; outputBit < 8; outputBit++ )
		{
			int pin = PARALLEL_PORT_OUTPUT_PINS[ outputBit ];
			if( pin >= 0 ) pinMode( pin, OUTPUT );
			if( pin >= 0 ) digitalWrite( pin, HIGH );
			delay( msecPerLED );
			if( pin >= 0 ) digitalWrite( pin, LOW );
		}
	}
}

#endif // __Utilities_H__
