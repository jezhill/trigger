// Written for Teensy 3.1
// (make sure correct board is selected in Tools menu)
// TriggerLibrary headers - to ensure Arduino IDE has access to them:
//    mklink /J %USERPROFILE%\Documents\Arduino\libraries\TriggerLibrary  ..\TriggerLibrary
#include "TriggerBoxV2.h"
#include "Utilities.h"
#include "Oscillator.h"

const float MIN_TRIGGER_DURATION_MSEC     =    20;
const int   TRIGGER_OUTPUT_PIN            =     PARALLEL_PORT_OUTPUT_PINS[ 0 ]; // D0
const int   LED_PIN                       =     BNC_PINS[ 0 ]; // if you want analog output, (a) unfortunately in TriggerBoxV2, the DAC pin (40, a.k.a. A14) is not broken out,
                                                               //                        but (b) in TriggerBoxV2, tip and ring of bottom TRS jack are capable of PWM output, so
                                                               //                                maybe use LED_PIN = TRS_RING_PINS[ 0 ], uncomment analogWriteResolution below,
                                                              //                                 and use analogWrite instead of digitalWrite below
const int   POWER_INDICATOR_LED           =     SPARE_LED_PINS[ 0 ];
const int   HIGH_WARNING_LED              =     SPARE_LED_PINS[ 1 ];

const int   SAMPLE_RATE_HZ                = 10000;
 
//////////////////////////////////////////////////////////////////////////////////////////////////////

double gFlickerCyclesPerSecond = 10;
unsigned int gCyclesPerTrigger = 10;
Oscillator gOscillator( gFlickerCyclesPerSecond, SAMPLE_RATE_HZ );

void setup()
{
  Serial.begin( 9600 );  
  Hello( 100, 2 );
  if( POWER_INDICATOR_LED >= 0 ) { pinMode( POWER_INDICATOR_LED, OUTPUT ); digitalWrite( POWER_INDICATOR_LED, HIGH ); }
  if( HIGH_WARNING_LED >= 0 )    { pinMode( HIGH_WARNING_LED,    OUTPUT ); digitalWrite( HIGH_WARNING_LED,    LOW  ); }
  pinMode( LED_PIN, OUTPUT );
  analogWriteResolution( 12 );
}

void loop()
{
  StartCommands();
  if( ProcessVariableCommand( "FlickerCyclesPerSecond", gFlickerCyclesPerSecond ) ) gOscillator.SetCyclesPerSecond( gFlickerCyclesPerSecond );
  if( ProcessVariableCommand( "CyclesPerTrigger", gCyclesPerTrigger ) ) {  }
  EndCommands();
  
  if( gOscillator.Tick() )
  {
    double t = gOscillator.Seconds();
    double waveform = ( gFlickerCyclesPerSecond == 0.0 ) ? 0.0 : sin( gOscillator.Radians() );
    digitalWrite( LED_PIN, ( waveform > 0 ) ? HIGH : LOW );
    //analogWrite( LED_PIN, ( int )( 0.5 + 4094.0 * 0.5 * waveform ) );
    bool triggerOn = gCyclesPerTrigger ? fmod( t, ( ( double )gCyclesPerTrigger / gFlickerCyclesPerSecond ) ) < MIN_TRIGGER_DURATION_MSEC / 1000.0 : false;
    digitalWrite( TRIGGER_OUTPUT_PIN, triggerOn ? HIGH : LOW );
  }
}


