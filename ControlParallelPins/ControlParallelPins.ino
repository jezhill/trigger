#include "Keyhole.h"

const int   SPARE_LED_PINS[]              = { 13,  7 }; // top and next-down-from-top (NB: 13 is also LED_BUILTIN)
const int   PARALLEL_PORT_OUTPUT_PINS[]   = {  8,  0,  1,  2,  3,  4,  5,  6 }; // D0 through D7 respectively

void setup()
{
  // put your setup code here, to run once:
  for( int i = 0; i < 8; i++ )
  {
    pinMode( PARALLEL_PORT_OUTPUT_PINS[ i ], OUTPUT );
    digitalWrite( PARALLEL_PORT_OUTPUT_PINS[ i ], LOW );
  }
  pinMode( SPARE_LED_PINS[ 0 ], OUTPUT );
  Keyhole::flicker( SPARE_LED_PINS[ 0 ] );
  digitalWrite( SPARE_LED_PINS[ 0 ], HIGH );
}

void loop()
{
  KEYHOLE k;
  if( k.begin() )
  {
    static bool d[ 8 ] = { 0, 0, 0, 0, 0, 0, 0, 0 };
    static int composite = 0;
    if( k.variable( "d", composite ) )
    {
      for( int i = 0; i < 8; i++ )
      {
        d[ i ] = ( composite & ( 1 << i ) ) != 0;
        digitalWrite( PARALLEL_PORT_OUTPUT_PINS[ i ], d[ i ] );
      }
    }
    composite = 0;
    static bool onebased = 1;
    k.variable( "onebased", onebased );
    for( int i = 0; i < 8; i++ )
    {
      static char name[ 3 ] = { 'd', 'x', '\0' };
      name[ 1 ] = '0' + onebased + i;
      if( k.variable( name, d[ i ] ) ) digitalWrite( PARALLEL_PORT_OUTPUT_PINS[ i ], d[ i ] );
      composite += ( int )( d[ i ] ) << i;
    }
    k.end();
  }

}
