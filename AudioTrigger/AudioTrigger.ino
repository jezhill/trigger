// Written for Teensy 3.1
// (make sure correct board is selected in Tools menu)
// TriggerLibrary headers - to ensure Arduino IDE has access to them:
//    mklink /J %USERPROFILE%\Documents\Arduino\libraries\TriggerLibrary  ..\TriggerLibrary
#include "TriggerBoxV2.h"
#include "TriggerProcessing.h"

const int   LEFT_AUDIO_INPUT_PIN          = TRS_TIP_PINS[ 1 ];  // top jack
const int   RIGHT_AUDIO_INPUT_PIN         = TRS_RING_PINS[ 1 ]; // top jack
const int   LEFT_TRIGGER_OUTPUT_PIN       = PARALLEL_PORT_OUTPUT_PINS[ 6 ]; // D6
const int   RIGHT_TRIGGER_OUTPUT_PIN      = PARALLEL_PORT_OUTPUT_PINS[ 7 ]; // D7
const int   LEFT_THRESHOLDING_INPUT_PIN   = POTENTIOMETER_PINS[ 0 ];
const int   RIGHT_THRESHOLDING_INPUT_PIN  = POTENTIOMETER_PINS[ 1 ];

const int   LEFT_BUTTON_INPUT_PIN         = RJ45_INPUT_PINS[ 0 ];
const int   RIGHT_BUTTON_INPUT_PIN        = RJ45_INPUT_PINS[ 1 ];
const int   LEFT_BUTTON_OUTPUT_PIN        = PARALLEL_PORT_OUTPUT_PINS[ 2 ];
const int   RIGHT_BUTTON_OUTPUT_PIN       = PARALLEL_PORT_OUTPUT_PINS[ 3 ];

const int   POWER_INDICATOR_LED           = SPARE_LED_PINS[ 0 ];
const int   HIGH_WARNING_LED              = SPARE_LED_PINS[ 1 ];

const float THRESHOLD_SCALING             =  65535.0 * POTENTIOMETER_SCALING;  // threshold signal value when potentiometer is turned up to max
const float MIN_TRIGGER_DURATION_MSEC     =    20;
const float MIN_TRIGGER_PERIOD_MSEC       =   430;

const int   SAMPLE_RATE_HZ                = 10000;      // Rate at which to sample the audio, in Hz
                                                        // NB: - Make sure this divides into 1 million
                                                        //     - If you change it, the filter coefficients
                                                        //       (hardcoded below for mHighPass and mLowPass)
                                                        //       will need to be recomputed.
                                       
const int   OUTPUT_RATE_HZ                =  1000;      // rate at which the potentiometer is scanned,
                                                        // the filtered envelope compared against the threshold,
                                                        // and the trigger output state updated.
                                                        // NB: - Make sure this divides into SAMPLE_RATE_HZ
 
//////////////////////////////////////////////////////////////////////////////////////////////////////

bool gSampleReady;
int gSampleCounter;
IntervalTimer gSamplingTimer;
void SamplingCallback() { gSampleReady = true; }

Trigger gLeftChannel(  1, LEFT_AUDIO_INPUT_PIN,  LEFT_TRIGGER_OUTPUT_PIN  );
Trigger gRightChannel( 2, RIGHT_AUDIO_INPUT_PIN, RIGHT_TRIGGER_OUTPUT_PIN );

void setup()
{
  Serial.begin( 38400 );
  analogReadResolution( 16 );
  analogReadAveraging( 4 );
  analogReference( DEFAULT );
 
  gLeftChannel.SetThresholding(  LEFT_THRESHOLDING_INPUT_PIN,  THRESHOLD_SCALING );
  gRightChannel.SetThresholding( RIGHT_THRESHOLDING_INPUT_PIN, THRESHOLD_SCALING );
  gLeftChannel.SetTiming(  MIN_TRIGGER_DURATION_MSEC, MIN_TRIGGER_PERIOD_MSEC, SAMPLE_RATE_HZ );
  gRightChannel.SetTiming( MIN_TRIGGER_DURATION_MSEC, MIN_TRIGGER_PERIOD_MSEC, SAMPLE_RATE_HZ );

  gSampleReady = false;
  gSampleCounter = 0;
  gSamplingTimer.begin( SamplingCallback, 1000000 / SAMPLE_RATE_HZ );
  
  for( int repeat = 0; repeat < 1; repeat++ )
  {
    for( int outputBit = 0; outputBit < 8; outputBit++ )
    {
      int pin = PARALLEL_PORT_OUTPUT_PINS[ outputBit ];
      if( pin >= 0 ) pinMode( pin, OUTPUT );
      if( pin >= 0 ) digitalWrite( pin, HIGH );
      delay( 200 );
      if( pin >= 0 ) digitalWrite( pin, LOW );
    }
  }
  if( POWER_INDICATOR_LED >= 0 ) { pinMode( POWER_INDICATOR_LED, OUTPUT ); digitalWrite( POWER_INDICATOR_LED, HIGH ); }
  if( HIGH_WARNING_LED >= 0 )    { pinMode( HIGH_WARNING_LED,    OUTPUT ); digitalWrite( HIGH_WARNING_LED,    LOW  ); }
  if( LEFT_BUTTON_INPUT_PIN   >= 0 ) pinMode( LEFT_BUTTON_INPUT_PIN,   INPUT_PULLUP );
  if( RIGHT_BUTTON_INPUT_PIN  >= 0 ) pinMode( RIGHT_BUTTON_INPUT_PIN,  INPUT_PULLUP );
  if( LEFT_BUTTON_OUTPUT_PIN  >= 0 ) pinMode( LEFT_BUTTON_OUTPUT_PIN,  OUTPUT );
  if( RIGHT_BUTTON_OUTPUT_PIN >= 0 ) pinMode( RIGHT_BUTTON_OUTPUT_PIN, OUTPUT );
}

void loop()
{
  if( LEFT_BUTTON_INPUT_PIN  >= 0 && LEFT_BUTTON_OUTPUT_PIN  >= 0 )
  {
    bool pressed = ( digitalRead( LEFT_BUTTON_INPUT_PIN ) == LOW );
    digitalWrite( LEFT_BUTTON_OUTPUT_PIN,  pressed ? HIGH : LOW );
    // digitalWrite( TRS_TIP_PINS[ 0 ], pressed ? HIGH : LOW ); // double-trigger
  }
  if( RIGHT_BUTTON_INPUT_PIN >= 0 && RIGHT_BUTTON_OUTPUT_PIN >= 0 )
  {
    bool pressed = ( digitalRead( RIGHT_BUTTON_INPUT_PIN ) == LOW );
    digitalWrite( RIGHT_BUTTON_OUTPUT_PIN, pressed ? HIGH : LOW );
    // digitalWrite( TRS_RING_PINS[ 0 ], pressed ? HIGH : LOW ); // double-trigger
  }
  
  if( gSampleReady )
  {
    gSampleReady = false;    
    gLeftChannel.ProcessSample();
    gRightChannel.ProcessSample();
    
    gSampleCounter = ( gSampleCounter + 1 ) % SAMPLE_RATE_HZ;
    if( gSampleCounter % ( SAMPLE_RATE_HZ / OUTPUT_RATE_HZ ) == 0 )
    {
      bool out = false;
      out = gLeftChannel.ProcessOutputCycle( out );
      // digitalWrite( BNC_PINS[ 0 ], out ? HIGH : LOW ); // double-trigger
      out = gRightChannel.ProcessOutputCycle( gRightChannel.OutputPin() == gLeftChannel.OutputPin() ? out : false );
      // digitalWrite( BNC_PINS[ 1 ], out ? HIGH : LOW ); // double-trigger
      if( gSampleCounter == 0 ) // report to serial port once per second
      {
        Serial.print( "{ " );
        gLeftChannel.Report();
        gRightChannel.Report();
        Serial.println( " }," );
      } // end once-per-second report 
    } // end trigger output cycle
  } // end sample
}
