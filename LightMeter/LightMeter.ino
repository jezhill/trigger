// Written for Teensy 3.1
// (make sure correct board is selected in Tools menu)
// TriggerLibrary headers - to ensure Arduino IDE has access to them:
//    mklink /J %USERPROFILE%\Documents\Arduino\libraries\TriggerLibrary  ..\TriggerLibrary
#include "TriggerBoxV2.h"
#include "Utilities.h"
#include "Oscillator.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////

const int   POWER_INDICATOR_LED         = SPARE_LED_PINS[ 0 ];
const int   HIGH_WARNING_LED            = SPARE_LED_PINS[ 1 ];
const int   HIGH_PIN                    = TRS_RING_PINS[ 0 ]; // bottom jack ring ("right") supplies power
const int   PHOTO_INPUT_PIN             = TRS_TIP_PINS[ 0 ];  // bottom jack tip ("left") reads light sensor signal
const int   THRESHOLDING_PIN            = POTENTIOMETER_PINS[ 0 ];
const float THRESHOLD_SCALING           = POTENTIOMETER_SCALING;

const int   OUTPUT_PIN                  = PARALLEL_PORT_OUTPUT_PINS[ 0 ];



//////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned long gLastFlickerTime = 0;
int gLastFlickerValue = 0;
void flicker( float periodMsec, float waitMsecBeforeReturn )
{
  if( HIGH_WARNING_LED < 0 ) return;
  unsigned long t, started = 0;
  do {
     t = millis();
     if( t < gLastFlickerTime || t > gLastFlickerTime + periodMsec / 2 )
     {
       gLastFlickerTime = t;
       digitalWrite( HIGH_WARNING_LED, gLastFlickerValue ? LOW : HIGH );
       gLastFlickerValue = 1 - gLastFlickerValue;
     }
     if( started == 0 ) started = t;
  } while( t - started  < waitMsecBeforeReturn );
}

const int SAMPLE_RATE_HZ = 480;
Oscillator gClock( 0, SAMPLE_RATE_HZ );
//////////////////////////////////////////////////////////////////////////////////////////////////////
void setup()
{
  Hello( 100, 1 );
  Serial.begin( 9600 );
  analogReadResolution( 16 );
  analogReadAveraging( 1 );
  analogReference( DEFAULT );
  pinMode( THRESHOLDING_PIN, INPUT );
  pinMode( PHOTO_INPUT_PIN, INPUT );
  pinMode( OUTPUT_PIN, OUTPUT );
  if( HIGH_PIN >= 0            ) { pinMode( HIGH_PIN,            OUTPUT ); digitalWrite( HIGH_PIN,            LOW  ); }
  if( POWER_INDICATOR_LED >= 0 ) { pinMode( POWER_INDICATOR_LED, OUTPUT ); digitalWrite( POWER_INDICATOR_LED, HIGH ); }
  if( HIGH_WARNING_LED >= 0    ) { pinMode( HIGH_WARNING_LED,    OUTPUT ); digitalWrite( HIGH_WARNING_LED,    HIGH ); flicker( 200, 5000 ); }
  // warning flicker is on, indicating that the ring of the lower audio jack will have 3.3V applied to it:
  // if that's going to be a problem, you have 5 seconds to unplug, before we do this...
  if( HIGH_PIN >= 0            ) { pinMode( HIGH_PIN,            OUTPUT ); digitalWrite( HIGH_PIN,            HIGH ); }
  
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
  flicker( 400, 0 );
  if( gClock.Tick() )
  {
    float photoVoltage = analogRead( PHOTO_INPUT_PIN ) / 1023.0;
    //float thresholdVoltage = analogRead( THRESHOLDING_PIN ) * THRESHOLD_SCALING / 1023.0;
    Serial.print( micros() ); // NB: 32-bit int wraps every 71 minutes 35 sec
    Serial.print( "," );
    Serial.print( photoVoltage, 4 );
    Serial.println( "," );
    Serial.flush();
  }  
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

