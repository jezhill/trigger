import sys
import ast
import time
import threading

import numpy
import matplotlib.pyplot as plt
import matplotlib.animation

try: import serial   # conda install pyserial
except ImportError: serial = None

class MeterError( Exception ): pass
class Meter( object ):
	def __init__( self, port, baud=9600, buffer=60000, threaded=True, callback=None ):
		self.port = port
		self.baud = baud
		if isinstance( buffer, int ): buffer = numpy.zeros( [ buffer, 2 ], dtype=float  ) + numpy.nan 
		self.buffer = buffer
		self.callback = callback
		
		self.device = None
		self.samples = 0
		self.cursor = 0
		self.cleared = False
		self.keep_going = False
		self.lock = threading.Lock()
		self.exc_info = None
	
		if threaded: self.Go( threaded=True )
		
	def Go( self, threaded=True ):
		if self.keep_going: return
		self.keep_going = True
		self.exc_info = None
		if not serial: raise ImportError( 'failed to import `serial` module - install with `pip install pyserial`' )
		if threaded:
			self.thread = threading.Thread( target=self.Main )
			self.thread.start()
		else:
			return self.Main()
			
	def Stop( self ):
		self.keep_going = False
	
	def Main( self ):
		try:
			self.keep_going = True
			self.cleared = False
			self.samples = self.cursor = 0
			with serial.Serial( self.port, self.baud ) as self.device:
				self.device.read_all()
				while self.keep_going:
					self.line = self.device.readline().decode( 'ascii' ).rstrip()
					self.reading = None
					if not self.line: continue
					with self.lock:
						try:
							try: self.reading = ast.literal_eval( self.line )
							except: raise MeterError()
							if self.buffer is not None:
								try: self.buffer[ self.cursor, : ] = self.reading[ -2: ]
								except: raise MeterError()
								self.cursor = ( self.cursor + 1 ) % len( self.buffer )
						except MeterError:
							if self.cleared: raise( ValueError( 'malformed reading %r' % self.line ) )
							else: continue
						self.samples += 1
						self.cleared = True
					if self.callback:
						self.callback( self.reading )
		except:
			self.exc_info = sys.exc_info()
		finally:
			self.keep_going = False
		
	def Data( self, nSeconds=None ):
		if self.buffer is None: return None
		with self.lock:
			cursor, samples = self.cursor, self.samples
			x = self.buffer.copy()
		x = numpy.concatenate( [ x[ cursor: ], x[ :cursor ] ], axis=0 )
		x = x[ -samples: ]
		if nSeconds is not None:
			t = ( x[ :, 0 ] - x[ 0, 0 ] ) / 1e6
			x = x[ t >= t[ -1 ] - nSeconds, : ]
		return x

	def PlotSamplingFrequency( self, nSeconds=1.0 ):
		data = self.Data( nSeconds=nSeconds )
		t, dt = Seconds( data )
		fs = 1.0 / dt
		plt.cla()
		plt.plot( t[ 1: ], fs, 'x-' )
		plt.grid( True )
	
	def PlotData( self, data, ylim=None ):
		if isinstance( data, ( int, float ) ): data = self.Data( nSeconds=data )
		t, dt = Seconds( data )
		plt.cla()
		plt.plot( t, data[ :, 1 ], 'x-' )
		if ylim is not None: plt.ylim( [ min( ylim ), max( ylim ) ] )
		plt.grid( True )

def UnwrapDiff( x, base, axis=None, startval=None, dtype=None ):
	"""
	Assume `X` is a wrapped version of an underlying value `Y` we're
	interested in. For example, it's a 16-bit value that wraps around
	at 65536, or it's an angle which wraps back to 0 at 2*pi.
	
	`base` is the value (65536 or 2*pi in the above examples) such that
	`X = Y % base`.  The default value of `base` is 2*pi.
	
	Let `dY` be the numeric diff of `Y` in dimension `axis`, computed
	from `X` by unwrapping in order to avoid jumps larger than `base/2`.
	Thus, with `base=65536`, a jump from 65535 to 1 is considered as a
	step of +2. With `base=360`, a jump from 10 to 350 is considered as
	a step of -20.
	
	`Y` is then reconstructed based on `dY` and `startval` (which
	defaults to the actual initial value(s) of `X`).
	
	Return value is `(dY,Y)`.
	"""
	if dtype is not None: x = numpy.asarray( x, dtype=dtype )
	if axis is None:
		axis, = numpy.where( numpy.array( x.shape ) != 1 )
		if len( axis ): axis = axis[ 0 ]
		else: axis = 0
	x = x % base
	d = numpy.diff( x, axis=axis )
	nj = d < -base / 2.0
	d[ nj ] += base
	pj = d > base / 2.0
	d[ pj ] -= base
	d[ numpy.isnan( d ) ] = 0
	sub = [ slice( None ) for i in x.shape ]
	sub[ axis ] = [ 0 ]
	sv = x[ tuple( sub ) ]
	if startval is not None: sv = sv * 0 + startval
	x = numpy.cumsum( numpy.concatenate( ( sv, d ), axis=axis ), axis=axis )
	return d, x

def Seconds( data ):
	t = data[ :, 0 ]
	if not t.size: return t, t
	dt, t = UnwrapDiff( t, 4294967296 )
	dt /= 1e6
	t /= 1e6
	t -= t[ -1 ]
	return t, dt

class Monitor( object ):

	def __init__( self, meter, nSeconds=3.0, period=1.0, ylim=None, flim=None, alim=None, dlim=None, normalize=False ):
		self.meter = meter
		self.lookbackInSeconds = nSeconds
		self.periodInSeconds = period
		self.ylim = ylim
		self.frequencyLimits = flim
		self.amplitudeLimits = alim
		self.dBLimits = dlim
		self.normalize = normalize
		self.animation = None
		self.timeAxes = None
		self.freqAxes = None
		self.Go()
		
	def Go( self ):	
	
		self.meter.Go()
		time.sleep( 0.5 )
		if not self.meter.keep_going: raise RuntimeError( 'meter is not running' )
		if 'IPython' in sys.modules: plt.ion()
		
		self.figure = plt.gcf()
		def close( event ):
			self.animation.event_source.stop()
			self.animation = None			
			self.meter.Stop()
		self.figure.canvas.mpl_connect( 'close_event', close )
		self.timeAxes = None
		self.freqAxes = None
		
		plt.gca() # strange matplotlib.animation feature: animation won't call its callback unless there's an axes object present to start (even if the axes object is not used)
		self.animation = matplotlib.animation.FuncAnimation( self.figure, self.Update, interval=self.periodInSeconds * 1000 )	
		if 'IPython' not in sys.modules: plt.show()
		
	def Pause( self, pause=True ):
		if not self.animation: return
		if pause: self.animation.event_source.stop()
		else:     self.animation.event_source.start()
	
	def Unpause( self ):
		return self.Pause( False )
		
	def Update( self, frame ):
		if not self.meter.keep_going: return []
		
		spectrum = self.frequencyLimits is not None or self.amplitudeLimits is not None or self.dBLimits is not None
		if spectrum and self.freqAxes is None:
			self.figure.clf()
			self.timeAxes = self.figure.add_subplot( 2, 1, 1 )
			self.freqAxes = self.figure.add_subplot( 2, 1, 2 )
		if self.timeAxes is None:
			self.timeAxes = self.figure.gca()
		
		data = self.meter.Data( nSeconds=self.lookbackInSeconds )

		plt.sca( self.timeAxes )
		self.meter.PlotData( data, ylim=self.ylim )
		plt.xlabel( 'Time (seconds)' )
		
		if spectrum and data.size:
			t, dt = Seconds( data )
			dt_mean = numpy.mean( dt )
			dt_std = numpy.std( dt )
			fs = int( round( 1.0 / dt_mean ) )
			interpolatedTime = numpy.linspace( 0, self.lookbackInSeconds, int( round( self.lookbackInSeconds * fs ) ), endpoint=False ) - self.lookbackInSeconds
			interpolatedData = numpy.interp( interpolatedTime, t, data[ :, 1 ] )
			#plt.plot( interpolatedTime, interpolatedData, '+-' ); plt.title( '%g Hz [%g, %g]' % ( fs, 1.0 / ( dt_mean + 1.96 * dt_std ), 1.0 / ( dt_mean - 1.96 * dt_std ) ) )
			if interpolatedData.size:
				interpolatedData -= interpolatedData.mean()
				dB = self.dBLimits is not None
				plim = self.dBLimits if self.dBLimits else self.amplitudeLimits
				baseValue = min( plim ) if plim else 0.0
				flim = self.frequencyLimits
				import snap.Signal
				ap = snap.Signal.Spectrum( interpolatedData, samplingfreq_hz=fs )
				ap[ 'signal' ] = interpolatedData
				self.spectrum = ap
				if self.normalize: ap[ 'amplitude' ] /= max( ap[ 'amplitude' ].flat )
				plt.sca( self.freqAxes )
				plt.cla()
				snap.Signal.PlotSpectrum( ap, dB=dB, baseValue=baseValue )
				plt.xlabel( 'Frequency (Hz)' )
				if flim: plt.xlim( [ min( flim ), max( flim ) ] )
				if plim: plt.ylim( [ min( plim ), max( plim ) ] )
		return [ ax for ax in [ self.timeAxes, self.freqAxes ] if ax is not None ]


if __name__ == '__main__':
	import argparse
	class pair( list ):
		def __init__( self, x ):
			if isinstance( x, str ): x = ast.literal_eval( x )
			list.__init__( self, x )
			if len( self ) != 2: raise ValueError( 'must have 2 elements' )
	parser = argparse.ArgumentParser()
	parser.add_argument( "-p", "--port", type=str, default='COM4' )
	parser.add_argument( "-b", "--baud", type=int, default=9600 )
	parser.add_argument( "-y", "--ylim", type=pair, default=None )
	parser.add_argument( "-s", "--seconds", type=float, default=3.0 )
	parser.add_argument( "-u", "--update-period", type=float, default=1.0 )
	parser.add_argument( "-f", "--flim", type=pair, default=None )
	parser.add_argument( "-a", "--alim", type=pair, default=None )
	parser.add_argument( "-d", "--dlim", type=pair, default=None )
	parser.add_argument( "-n", "--normalize", type=bool, nargs='?', const=True, default=False )
	args = parser.parse_args()
	meter = Meter( port=args.port, baud=args.baud )
	monitor = Monitor( meter, nSeconds=args.seconds, ylim=args.ylim, period=args.update_period, flim=args.flim, alim=args.alim, dlim=args.dlim, normalize=args.normalize )
	