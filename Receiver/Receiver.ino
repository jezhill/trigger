#include "Keyhole.h"

int gTriggerPin = 0;
unsigned int gOnsetTimeInMicros;

void setup( void )
{
  pinMode( LED_BUILTIN, OUTPUT );
  if( gTriggerPin >= 0 ) pinMode( gTriggerPin, INPUT );
  Serial.begin( 115200 );
  Keyhole::flicker( LED_BUILTIN, 5, 95, 1000 );
}
void loop( void )
{
  static int input = 0;
  int newInput = 0;
  unsigned int now = micros();
  if( gTriggerPin >= 0 ) newInput = digitalRead( gTriggerPin );
  if( newInput != input )
  {
    digitalWrite( LED_BUILTIN, newInput );
    if( newInput ) gOnsetTimeInMicros = now;
    else
    {
      unsigned int elapsedMicros = now - gOnsetTimeInMicros;
      Serial.print( "PulseDurationMsec " );
      Serial.print( ( elapsedMicros + 500 ) / 1000 );
      Serial.println( " 0" );
      Serial.flush();
    }
    input = newInput;
  }

  KEYHOLE keyhole( Serial );
  if( keyhole.begin() )
  {
    keyhole.variable( "pin", gTriggerPin );
    keyhole.variable( "input", input, VARIABLE_READ_ONLY );
    keyhole.end();
  }
}