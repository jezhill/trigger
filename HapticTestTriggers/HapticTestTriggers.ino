#include "Keyhole.h"
#include "RecentMax.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


class Trigger
{
  public:
    Trigger(int inputPin, int outputPin, int ledPin = -1, const String & name="trigger" )
      : mLastSampleTimeInMicros(0),
        mSamplePeriodInMicros(100),
        mInputPin(inputPin),
        mPreviousReading(0),
        mRecentMaxAbsDiff(0),
        mThreshold(200),
        mOutputPin(outputPin),
        mLEDPin(ledPin),
        mMinimumPulseDurationInMillis(10),
        mPulseDurationIncrementInMicros(10000),
        mRefractoryPeriodInMillis(200),
        mPulseOn(false),
        mInRefractoryPeriod(false),
        mTimeLastExceeded(0),
        mLastPulseStartTimeInMicros(0),
        mLastPulseFinishTimeInMicros(0),
        mLastPulseDurationInMicros(0),
        mPulsesStarted(0),
        mPulsesFinished(0),
        mSlidingWindow(1000, 20),
        mPulseStartCallback(NULL),
        mPulseFinishCallback(NULL),
        mName( name )
    {
      // Assuming mSamplePeriodInMicros is 100, each 1000-sample bin is 100ms.
      // Multiply that by 20 bins, and you get a total memory span of 2 seconds
      // in mSlidingWindow (although one bin always gets emptied when all bins
      // are full, so the memory span actually fluctuates between 1.9 and 2
      // seconds).

      if (mInputPin >= 0) pinMode(mInputPin, INPUT);
      if (mOutputPin >= 0) pinMode(mOutputPin, OUTPUT);
      if (mLEDPin >= 0) pinMode(mLEDPin, OUTPUT);
    }
    ~Trigger() { }

    unsigned long mLastSampleTimeInMicros;
    unsigned long mSamplePeriodInMicros;
    int mInputPin;
    int mPreviousReading;
    int mRecentMaxAbsDiff;
    int mThreshold;
    int mOutputPin;
    int mLEDPin;
    unsigned long mMinimumPulseDurationInMillis;
    unsigned long mPulseDurationIncrementInMicros;
    unsigned long mRefractoryPeriodInMillis;
    bool mPulseOn;
    bool mInRefractoryPeriod;
    unsigned long mTimeLastExceeded;
    unsigned long mLastPulseStartTimeInMicros;
    unsigned long mLastPulseFinishTimeInMicros;
    unsigned long mLastPulseDurationInMicros;
    unsigned long mPulsesStarted;
    unsigned long mPulsesFinished;
    RecentMax<int> mSlidingWindow;
    void (*mPulseStartCallback)( Trigger & t );
    void (*mPulseFinishCallback)( Trigger & t );
    String mName;

    bool process( unsigned long t )
    {
      if( t - mLastSampleTimeInMicros < mSamplePeriodInMicros ) return mPulseOn;
      mLastSampleTimeInMicros = t;
      int currentReading = 0;
      if( mInputPin >= 0 ) currentReading = analogRead( mInputPin );
      int absDiff = abs( currentReading - mPreviousReading );
      mPreviousReading = currentReading;

      unsigned long microsSinceLastPulseStart = t - mLastPulseStartTimeInMicros;

      // If we think we're still in the refractory period, but the refractory duration has
      // in fact elapsed, mark the refractory period as finished:
      if( mInRefractoryPeriod && microsSinceLastPulseStart >= mRefractoryPeriodInMillis * 1000 ) mInRefractoryPeriod = false;


      bool exceeded = absDiff > mThreshold;
      if( exceeded ) mTimeLastExceeded = t;

      if( mPulseOn )
      {
        if( microsSinceLastPulseStart >= mMinimumPulseDurationInMillis * 1000 && t - mTimeLastExceeded >= mPulseDurationIncrementInMicros )
        { // The pulse always lasts 7--9 ms longer than the input both nominally (duration computed here) and objectively (when recorded with nidaq).
          // The extra duration depends on both the frequency content of the input, and the mPulseDurationIncrementInMicros (but this cannot be made much shorter than 10ms)
          if( mOutputPin >= 0 ) digitalWrite( mOutputPin, LOW );
          if( mLEDPin >= 0 )    digitalWrite( mLEDPin,    LOW );
          mPulseOn = false;
          mPulsesFinished++;
          mLastPulseFinishTimeInMicros = t;
          mLastPulseDurationInMicros = microsSinceLastPulseStart;
          if( mPulseFinishCallback ) mPulseFinishCallback( *this );
        }
      }
      else
      {
        if( !mInRefractoryPeriod && exceeded )
        {
          if( mOutputPin >= 0 ) digitalWrite( mOutputPin, HIGH );
          if( mLEDPin >= 0 )    digitalWrite( mLEDPin,    HIGH );
          mPulseOn = true;
          mPulsesStarted++;
          mLastPulseStartTimeInMicros = t;
          if( mRefractoryPeriodInMillis > 0 ) mInRefractoryPeriod = true;
          if( mPulseStartCallback ) mPulseStartCallback( *this );
        }
      }

      mRecentMaxAbsDiff = mSlidingWindow.process( absDiff );

      return mPulseOn;
    }
};

static Trigger gTone( 37, 27, 0, "ResponseTone" ); // ortiz box: response tones from phone cable -> ring of top jack    -> right BNC out
static Trigger gBuzz( 23, 30, 8, "Buzz" );         // ortiz box: mic signal                      -> ring of bottom jack -> left BNC out 
//static Trigger gTone( A1, 8, -1 ); // arduino uno audio jack -> BNC out 
//static Trigger gBuzz( A0, 9, 10 ); // arduino uno hardwired mic -> BNC out

void AnnounceStart( Trigger & t )
{
  Serial.print( t.mName );
  Serial.println( " 1" );
  Serial.flush();
}
void AnnounceFinish( Trigger & t )
{
  Serial.print( t.mName );
  Serial.println( " 0" );

  Serial.print( t.mName );
  Serial.print( "Msec " );
  Serial.print( ( int )( 0.5 + t.mLastPulseDurationInMicros / 1000.0 ) );
  Serial.println( " 0" );
  Serial.flush();
}

void setup()
{
  const int MIC_SUPPLY = 22; // ortiz box: tip of bottom jack -> mic supply voltage
  if( MIC_SUPPLY >= 0 ) { pinMode( MIC_SUPPLY, OUTPUT ); digitalWrite( MIC_SUPPLY, HIGH ); } // TODO: remove
  
  gTone.mThreshold =  15;
  gBuzz.mThreshold = 200;

  gTone.mPulseStartCallback  = AnnounceStart;
  gBuzz.mPulseStartCallback  = AnnounceStart;
  gTone.mPulseFinishCallback = AnnounceFinish;
  gBuzz.mPulseFinishCallback = AnnounceFinish;

  Serial.begin(9600);
  Keyhole::flicker( LED_BUILTIN, 100, 50 );
}

void loop()
{
  unsigned long t = micros();
  static unsigned long sampleInterval = 0, processingTime = 0, sampleTime = gBuzz.mLastSampleTimeInMicros;
  gTone.process( t );
  gBuzz.process( t );
  if( sampleTime != gBuzz.mLastSampleTimeInMicros )
  {
    processingTime = micros() - t;
    sampleInterval = gBuzz.mLastSampleTimeInMicros - sampleTime;
    sampleTime = gBuzz.mLastSampleTimeInMicros;
  }

  KEYHOLE keyhole( Serial );
  if( keyhole.begin() )
  {
    keyhole.variable( "delta",   sampleInterval, VARIABLE_READ_ONLY );
    keyhole.variable( "process", processingTime, VARIABLE_READ_ONLY );
    
    keyhole.variable( "trecent", gTone.mRecentMaxAbsDiff, VARIABLE_READ_ONLY );
    keyhole.variable( "tthresh", gTone.mThreshold );
    keyhole.variable( "tpulse",  gTone.mMinimumPulseDurationInMillis );
    keyhole.variable( "trefrac", gTone.mRefractoryPeriodInMillis );
    keyhole.variable( "tinc",    gTone.mPulseDurationIncrementInMicros );

    keyhole.variable( "brecent", gBuzz.mRecentMaxAbsDiff, VARIABLE_READ_ONLY );
    keyhole.variable( "bthresh", gBuzz.mThreshold );
    keyhole.variable( "bpulse",  gBuzz.mMinimumPulseDurationInMillis );
    keyhole.variable( "brefrac", gBuzz.mRefractoryPeriodInMillis );
    keyhole.variable( "binc",    gBuzz.mPulseDurationIncrementInMicros );

    if( keyhole.command( "plot" ) ) keyhole.plotterMode = !keyhole.plotterMode;
    if( keyhole.command( "auto" ) ) keyhole.autoSeconds = keyhole.autoSeconds ? 0.0 : 1.0;
    if( keyhole.command( "publish" ) )
    {
      // BCI2000 definitions
      Serial.println( "ResponseTone      1 0 0 0" );
      Serial.println( "ResponseToneMsec 15 0 0 0" );
      Serial.println( "Buzz              1 0 0 0" );
      Serial.println( "BuzzMsec         15 0 0 0" );
      Serial.println(); // blank line to conclude definitions
    }
    keyhole.end();
  }
}
