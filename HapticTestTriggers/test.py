
def f( ms=200, hz=500, **kwargs ):
	import audiomath as am
	am.Player( am.Sound( fs=44100 ).GenerateWaveform( duration_msec=ms, freq_hz=hz, phase_deg=90 ) ).Play( wait=1, **kwargs )
	
def n( ms=200, **kwargs ):
	import audiomath as am
	am.Player( am.Sound( fs=44100 ).GenerateWaveform( duration_msec=ms, waveform=am.Signal.Noise ) ).Play( wait=1, **kwargs )

def analyze( directory='c:/neurotech/epocs/app/data/Name001', index=-1, threshold=None ):
	from BCI2000Tools.FileReader import bcistream, DivertWarnings; DivertWarnings( [] )
	from BCI2000Tools.Numerics import edges
	import numpy
	b = bcistream( directory, index )
	print( b )
	signal, states = b.decode()
	signal = signal.A[ 0 ]
	if threshold is None: threshold = signal.max() / 5
	finish = numpy.array( edges( signal > threshold, direction=-1 ) )
	start  = numpy.array( edges( signal > threshold, direction=+1 )[ : finish.size ] )
	return b.samples2msec( finish - start )
