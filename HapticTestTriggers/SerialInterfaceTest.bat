#! ../prog/BCI2000Shell
@cls & C:\neurotech\bci2000-svn\HEAD\build\build-Qt-6.2.3-msvc2019_64\BCI2000\prog\BCI2000Shell %0 %* #! && exit /b 0 || exit /b 1\n


change directory $BCI2000LAUNCHDIR
show window
set title ${extract file base $0}
reset system

set environment SERIALPORT COM12  # change this default value as needed...
if [ $1 ]; set environment SERIALPORT $1; end # ...or override it by supplying it as a command-line argument to the batch file

startup system localhost
start executable SignalGenerator       --local --SerialPort=${SERIALPORT}  --PublishCommand=publish\n
start executable DummySignalProcessing --local
start executable DummyApplication      --local

wait for connected

set parameter SamplingRate    1000
set parameter SampleBlockSize   40


visualize watch ResponseTone
visualize watch ResponseToneMsec
visualize watch Buzz
visualize watch BuzzMsec

setconfig
set state Running 1
