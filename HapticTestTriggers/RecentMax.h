template<typename NUMERICTYPE> class RecentMax
{
	public:
		RecentMax( unsigned long samplesPerBin, unsigned long numberOfBins ) :
			mSamplesPerBin( samplesPerBin ),
			mNumberOfBins( numberOfBins ),
			mSampleNumber( 0 ),
			mBestValues( new NUMERICTYPE[ numberOfBins ] )
		{
			
		}
		~RecentMax()
		{
			delete [] mBestValues;
		}
		void resize( unsigned long samplesPerBin, unsigned long numberOfBins )
		{
			mSamplesPerBin = samplesPerBin;
			mNumberOfBins = numberOfBins;
			mSampleNumber = 0;
			delete [] mBestValues;
			mBestValues = new NUMERICTYPE[ numberOfBins ];
		}
		NUMERICTYPE process( NUMERICTYPE signalValue )
		{
			if( mSampleNumber >= mSamplesPerBin * mNumberOfBins ) mSampleNumber = 0; // wrap around (the bins effectively form a ring buffer)
			unsigned long binIndex = mSampleNumber / mSamplesPerBin;
			if( mSampleNumber % mSamplesPerBin == 0 ) mBestValues[ binIndex ] = signalValue;  // first sample of bin? reset the bin
			else mBestValues[ binIndex ] = betterValue( mBestValues[ binIndex ], signalValue );  // otherwise compute the max
			NUMERICTYPE overallBest = mBestValues[ 0 ];
			for( unsigned long iBin = 1; iBin < mNumberOfBins; iBin++ )
				overallBest = betterValue( overallBest, mBestValues[ iBin ] );
			mSampleNumber++;
			return overallBest;
		}
		NUMERICTYPE betterValue( NUMERICTYPE a, NUMERICTYPE b )
		{
			return ( a > b ? a : b );
		}
		
	private:
		unsigned long mSamplesPerBin;
		unsigned long mNumberOfBins;
		unsigned long mSampleNumber;
		NUMERICTYPE * mBestValues;
};
