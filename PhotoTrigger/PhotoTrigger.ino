// Written for Teensy 3.1
// (make sure correct board is selected in Tools menu)
// TriggerLibrary headers - to ensure Arduino IDE has access to them:
//    mklink /J %USERPROFILE%\Documents\Arduino\libraries\TriggerLibrary  ..\TriggerLibrary
#include "TriggerBoxV2.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////

const int   POWER_INDICATOR_LED         = SPARE_LED_PINS[ 0 ];
const int   HIGH_WARNING_LED            = SPARE_LED_PINS[ 1 ];
const int   HIGH_PIN                    = TRS_RING_PINS[ 0 ]; // bottom jack ring ("right") supplies power
const int   PHOTO_INPUT_PIN             = TRS_TIP_PINS[ 0 ];  // bottom jack tip ("left") reads light sensor signal
const int   THRESHOLDING_PIN            = POTENTIOMETER_PINS[ 0 ];
const float THRESHOLD_SCALING           = POTENTIOMETER_SCALING;
const int   OUTPUT_PIN                  = PARALLEL_PORT_OUTPUT_PINS[ 0 ];

const int   LEFT_BUTTON_INPUT_PIN         = RJ45_INPUT_PINS[ 0 ];
const int   RIGHT_BUTTON_INPUT_PIN        = RJ45_INPUT_PINS[ 1 ];
const int   LEFT_BUTTON_OUTPUT_PIN        = PARALLEL_PORT_OUTPUT_PINS[ 2 ];
const int   RIGHT_BUTTON_OUTPUT_PIN       = PARALLEL_PORT_OUTPUT_PINS[ 3 ];


//////////////////////////////////////////////////////////////////////////////////////////////////////

unsigned long gLastFlickerTime = 0;
int gLastFlickerValue = 0;
void flicker( float periodMsec, float durationMsec )
{
  if( HIGH_WARNING_LED < 0 ) return;
  unsigned long t, started = 0;
  do {
     t = millis();
     if( t < gLastFlickerTime || t > gLastFlickerTime + periodMsec / 2 )
     {
       gLastFlickerTime = t;
       digitalWrite( HIGH_WARNING_LED, gLastFlickerValue ? LOW : HIGH );
       gLastFlickerValue = 1 - gLastFlickerValue;
     }
     if( started == 0 ) started = t;
  } while( t - started  < durationMsec );
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
void setup()
{
  Serial.begin( 38400 );
  analogReadResolution( 10 );
  analogReadAveraging( 4 );
  analogReference( DEFAULT );
  pinMode( THRESHOLDING_PIN, INPUT );
  pinMode( PHOTO_INPUT_PIN, INPUT );
  pinMode( OUTPUT_PIN, OUTPUT );
  if( HIGH_PIN >= 0            ) { pinMode( HIGH_PIN,            OUTPUT ); digitalWrite( HIGH_PIN,            LOW  ); }
  if( POWER_INDICATOR_LED >= 0 ) { pinMode( POWER_INDICATOR_LED, OUTPUT ); digitalWrite( POWER_INDICATOR_LED, HIGH ); }
  if( HIGH_WARNING_LED >= 0    ) { pinMode( HIGH_WARNING_LED,    OUTPUT ); digitalWrite( HIGH_WARNING_LED,    HIGH ); flicker( 200, 5000 ); }
  if( HIGH_PIN >= 0            ) { pinMode( HIGH_PIN,            OUTPUT ); digitalWrite( HIGH_PIN,            HIGH ); }
  
  if( LEFT_BUTTON_INPUT_PIN   >= 0 ) pinMode( LEFT_BUTTON_INPUT_PIN,   INPUT_PULLUP );
  if( RIGHT_BUTTON_INPUT_PIN  >= 0 ) pinMode( RIGHT_BUTTON_INPUT_PIN,  INPUT_PULLUP );
  if( LEFT_BUTTON_OUTPUT_PIN  >= 0 ) pinMode( LEFT_BUTTON_OUTPUT_PIN,  OUTPUT );
  if( RIGHT_BUTTON_OUTPUT_PIN >= 0 ) pinMode( RIGHT_BUTTON_OUTPUT_PIN, OUTPUT );
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
void loop()
{
  if( LEFT_BUTTON_INPUT_PIN  >= 0 && LEFT_BUTTON_OUTPUT_PIN  >= 0 ) digitalWrite( LEFT_BUTTON_OUTPUT_PIN,  ( digitalRead( LEFT_BUTTON_INPUT_PIN  ) == LOW ) ? HIGH : LOW );
  if( RIGHT_BUTTON_INPUT_PIN >= 0 && RIGHT_BUTTON_OUTPUT_PIN >= 0 ) digitalWrite( RIGHT_BUTTON_OUTPUT_PIN, ( digitalRead( RIGHT_BUTTON_INPUT_PIN ) == LOW ) ? HIGH : LOW );
  
  flicker( 200, 0 );
  float photoVoltage = analogRead( PHOTO_INPUT_PIN ) / 1023.0;
  float thresholdVoltage = analogRead( THRESHOLDING_PIN ) * THRESHOLD_SCALING / 1023.0;
  digitalWrite( OUTPUT_PIN, photoVoltage >= thresholdVoltage );
  if( 0 )
  {
    Serial.print("Light    : ");
    Serial.println(photoVoltage);
    Serial.print("Threshold: ");
    Serial.println(thresholdVoltage);
    delay(1000);
  }  
}
//////////////////////////////////////////////////////////////////////////////////////////////////////

