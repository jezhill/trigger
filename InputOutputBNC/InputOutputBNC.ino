/*
Allows you to issue output pulses of variable duration on
one BNC, and to detect input pulses on the other BNC and
measure their duration, to within certain limitations on
time resolution. Performance values below were measured
on a Teensy 3.1, compiled on Arduino 1.8.13 and
Teensyduino 1.53 on Ubuntu 20.04 (slower when compiled
on a MacBook with slightly earlier versions).

1. Production version (resolution ~ 3.2us per cycle):

   - Send any numeric value to have that assigned to
     `pulse` (see below) to control the output: negative
     for indefinite high voltage, zero for low voltage, and
     any positive number of microseconds to issue a pulse.

   - Send "r" to see a report of `resolution` (see below)
     reporting the average cycle duration in the most-
     recently-completed 2-second segment.

   In the production version, when you connect the output
   and input BNCs together, the shortest pulse that you can
   reliably issue and detect is about 7 microseconds. 

2. SerialInterface.h version (used for development only, as it
   has poorer time resolution of about 12us per cycle, due to
   the SerialInterface overhead)::

       # Read/write variables to query/reassign pins:
       
         output_pin
         input_pin
         output_led
         input_led

       # Other read/write variables, with examples of use:
       
         pulse=0     # turn output off
         pulse=-1    # turn output on indefinitely
         pulse=4e6   # start a pulse (duration in microseconds)
      
         output=0    # turn output off
         output=1    # turn output on

       # Read-only variables:
       
         input       # current input level (0 or 1)
         resolution  # average microseconds per cycle during the
                     # last complete 2-second segment
    
*/

//#include "SerialInterface.h" // used in development only. comment out for final (performance-optimized) version

#include "TriggerBoxV2.h"     // ln -sfn ~/code/Trigger/TriggerLibrary ~/Arduino/libraries/
const int   POWER_INDICATOR_LED   =    SPARE_LED_PINS[ 0 ];            // i.e. topmost LED in strip.  Change to `LED_BUILTIN` to use the onboard LED.
const int   DEFAULT_OUTPUT_PIN    =    BNC_PINS[ 0 ];                  // i.e. the left BNC
const int   DEFAULT_OUTPUT_LED    =    PARALLEL_PORT_OUTPUT_PINS[ 0 ]; // i.e. D0 and its corresponding LED
const int   DEFAULT_INPUT_PIN     =    BNC_PINS[ 1 ];                  // i.e. the right BNC
const int   DEFAULT_INPUT_LED     =    PARALLEL_PORT_OUTPUT_PINS[ 3 ]; // i.e. D3 and its corresponding LED

const int   BAUD                  =    38400;  // Serial port communication rate (probably ignored, for USB connections)

//////////////////////////////////////////////////////////////////////////////////////////////////////

int   gOutputValue;
int   gInputValue;
int   gOutputPin;
int   gInputPin;
int   gOutputLed;
int   gInputLed;

double gOutputPulseDurationMicroseconds;
unsigned long gOutputPulseStopMicros;
unsigned long gInputPulseStartMicros;

double gMicrosecondsPerCycle;
unsigned long gTimerStartMicros;
unsigned long gCyclesPerformedSinceTimerStart;


void ReadyPin( int pin, int mode, int initialValue = LOW )
{
  if ( pin < 0 ) return;
  pinMode( pin, mode );
  if ( mode == OUTPUT ) digitalWrite( pin, initialValue );
}

void Output( int value )
{
  if ( gOutputPin >= 0 ) digitalWrite( gOutputPin, value );
  if ( gOutputLed >= 0 ) digitalWrite( gOutputLed, value );
  gOutputValue = value;
}

void StartTimedOutput( double outputPulseDurationMicroseconds )
{
  Output( outputPulseDurationMicroseconds ? HIGH : LOW );
  if( gOutputPin < 0 || outputPulseDurationMicroseconds <= 0 ) gOutputPulseStopMicros = 0;
  else gOutputPulseStopMicros = micros() + ( unsigned long )( 0.5 + outputPulseDurationMicroseconds );
}

void setup()
{
  Serial.begin( BAUD );
  if ( POWER_INDICATOR_LED >= 0 )
  { // Blink power LED for a couple of seconds to indicate that the unit has been restarted
    pinMode( POWER_INDICATOR_LED, OUTPUT );
    for ( int repeat = 0; repeat < 10; repeat++ )
    {
      delay( 100 );
      digitalWrite( POWER_INDICATOR_LED, LOW );
      delay( 150 );
      digitalWrite( POWER_INDICATOR_LED, HIGH );
    }
  }

  gOutputPin = DEFAULT_OUTPUT_PIN;
  gOutputLed = DEFAULT_OUTPUT_LED;
  gInputPin  = DEFAULT_INPUT_PIN;
  gInputLed  = DEFAULT_INPUT_LED;

  gOutputValue = LOW;
  ReadyPin( gOutputPin, OUTPUT, gOutputValue );
  ReadyPin( gOutputLed, OUTPUT, gOutputValue );
  ReadyPin( gInputPin,   INPUT );
  gInputValue = ( gInputPin >= 0 ) ? digitalRead( gInputPin ) : LOW;
  ReadyPin( gInputLed,  OUTPUT, gInputValue );
  
  gOutputPulseDurationMicroseconds = 0.0;
  gInputPulseStartMicros = 0;
  gOutputPulseStopMicros = 0;

  gMicrosecondsPerCycle = -1.0;
  gCyclesPerformedSinceTimerStart = 0;
  gTimerStartMicros = millis();
}

void loop()
{
  unsigned long cycleStartTime = micros();
  if ( gOutputPulseStopMicros && ( long )( cycleStartTime - gOutputPulseStopMicros ) > 0 )
  {
    gOutputPulseStopMicros = 0;
    Output( LOW );
  }
  if ( gInputPin >= 0 )
  {
    int latest = digitalRead( gInputPin );
    if ( gInputValue != latest )
    {
      if ( latest == LOW )
      {
        Serial.printf( "{'InputPulseFinishTimeInSeconds': %8.3f, 'InputPulseDurationInMicroseconds': %lu}\n", millis() / 1000.0, ( unsigned long )( micros() - gInputPulseStartMicros ) );
        Serial.flush();
      }
      else gInputPulseStartMicros = cycleStartTime;
      if ( gInputLed >= 0 ) digitalWrite( gInputLed, latest );
      gInputValue = latest;
    }
  }

# ifdef __SerialInterface_H__
    if( StartCommands() )
    {
      if ( ProcessVariableCommand( "output",   gOutputValue ) ) Output( gOutputValue );
      if ( ProcessVariableCommand( "output_pin", gOutputPin ) ) ReadyPin( gOutputPin, OUTPUT, gOutputValue );
      if ( ProcessVariableCommand( "input_pin",  gInputPin  ) ) ReadyPin( gInputPin,  INPUT );
      if ( ProcessVariableCommand( "output_led", gOutputLed ) ) ReadyPin( gOutputLed, OUTPUT, gOutputValue );
      if ( ProcessVariableCommand( "input_led",  gInputLed  ) ) ReadyPin( gInputLed,  OUTPUT, gInputValue  );
      if ( ProcessVariableCommand( "pulse", gOutputPulseDurationMicroseconds ) ) StartTimedOutput( gOutputPulseDurationMicroseconds );
      ProcessVariableCommand( "resolution", gMicrosecondsPerCycle, VARIABLE_READ_ONLY );
      ProcessVariableCommand( "input", gInputValue, VARIABLE_READ_ONLY );
      EndCommands();
    }
# else
    static String s;
    while ( Serial.available() )
    {
      char c = Serial.read();
      if ( c == '\n' )
      {
        char * remainder = NULL;
        gOutputPulseDurationMicroseconds = strtod( s.c_str(), &remainder );
        while ( remainder && isspace( *remainder ) ) remainder++;
        if ( !remainder || !*remainder ) StartTimedOutput( gOutputPulseDurationMicroseconds );
        else if( s == "r" ) { Serial.printf( "{'MicrosecondsPerCycle': %g}\n", gMicrosecondsPerCycle ); Serial.flush(); }
        else { Serial.printf( "{'SERIAL_COMMAND_ERROR': 'unrecognized input'}\n" ); Serial.flush(); }
        s = "";
      }
      else s += c;
    }
# endif
  unsigned long elapsed = cycleStartTime - gTimerStartMicros; // but don't increment gCyclesPerformedSinceTimerStart yet: this is time elapsed until the *start* of this loop, before the current cycle was performed
  if( elapsed >= 2000000 )
  {
    gMicrosecondsPerCycle = ( double )elapsed / ( double )gCyclesPerformedSinceTimerStart;
    gCyclesPerformedSinceTimerStart = 0;
    gTimerStartMicros = cycleStartTime;
  }
  gCyclesPerformedSinceTimerStart++;
}
