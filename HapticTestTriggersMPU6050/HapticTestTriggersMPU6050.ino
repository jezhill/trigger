#include "Keyhole.h"
#include "RecentMax.h"
#include "Wire.h"
#include "Triggering.h"

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



static MPU6050Trigger gBuzz(     0, LED_BUILTIN, "Buzz" );
static Trigger        gTone( 15, 5, -1,          "ResponseTone" );
//           audio input pin--^  
//        trigger output pin-----^
//        associated LED pin--------^

void AnnounceStart(Trigger& t)
{
  Serial.print(t.mName);
  Serial.println(" 1");
  Serial.flush();
}
void AnnounceFinish(Trigger& t)
{
  Serial.print(t.mName);
  Serial.println(" 0");

  Serial.print(t.mName);
  Serial.print("Msec ");
  Serial.print((int)(0.5 + t.mLastPulseDurationInMicros / 1000.0));
  Serial.println(" 0");
  Serial.flush();
}

void setup() {
  //const int MIC_SUPPLY = 22; // ortiz box: tip of bottom jack -> mic supply voltage
  //if( MIC_SUPPLY >= 0 ) { pinMode( MIC_SUPPLY, OUTPUT ); digitalWrite( MIC_SUPPLY, HIGH ); } // TODO: remove

  gBuzz.begin();
  gBuzz.mThreshold = 10000;
  gBuzz.mMinimumPulseDurationInMillis   =  50;
  gBuzz.mRefractoryPeriodInMillis       = 280;
  gBuzz.mPulseDurationIncrementInMicros =  10000;

  gTone.mSamplePeriodInMicros           =   1600;   // 625 Hz to match the accelerometer, since we're single-threaded
  gTone.mSlidingWindow.resize(63, 20);              // roughly 2s memory, again to match the accelerometer
  gTone.mThreshold = 15;

  // comment these out for production: MPU6050's synchronous sampling leaves very little time for Serial messaging
  //gTone.mPulseStartCallback  = AnnounceStart;
  //gTone.mPulseFinishCallback = AnnounceFinish;
  //gBuzz.mPulseStartCallback  = AnnounceStart;
  //gBuzz.mPulseFinishCallback = AnnounceFinish;

  Serial.begin(115200);
  Keyhole::flicker(LED_BUILTIN, 100, 50);
}

void loop() {
  unsigned long t = micros();
  static unsigned long sampleInterval = 0, processingTime = 0, sampleTime = gBuzz.mLastSampleTimeInMicros;
  gTone.process(t);
  gBuzz.process(t);
  if (sampleTime != gBuzz.mLastSampleTimeInMicros) {
    processingTime = micros() - t;
    sampleInterval = gBuzz.mLastSampleTimeInMicros - sampleTime;
    sampleTime = gBuzz.mLastSampleTimeInMicros;
  }

  KEYHOLE keyhole(Serial);
  if (keyhole.begin()) {
    keyhole.variable("delta", sampleInterval, VARIABLE_READ_ONLY);
    keyhole.variable("process", processingTime, VARIABLE_READ_ONLY);

    keyhole.variable("brecent", gBuzz.mRecentMaxAbsDiff, VARIABLE_READ_ONLY);
    keyhole.variable("bthresh", gBuzz.mThreshold);
    keyhole.variable("bpulse", gBuzz.mMinimumPulseDurationInMillis);
    keyhole.variable("brefrac", gBuzz.mRefractoryPeriodInMillis);
    keyhole.variable("binc", gBuzz.mPulseDurationIncrementInMicros);

    keyhole.variable("trecent", gTone.mRecentMaxAbsDiff, VARIABLE_READ_ONLY);
    keyhole.variable("tthresh", gTone.mThreshold);
    keyhole.variable("tpulse", gTone.mMinimumPulseDurationInMillis);
    keyhole.variable("trefrac", gTone.mRefractoryPeriodInMillis);
    keyhole.variable("tinc", gTone.mPulseDurationIncrementInMicros);

    if (keyhole.command("plot")) keyhole.plotterMode = !keyhole.plotterMode;
    if (keyhole.command("auto")) keyhole.autoSeconds = keyhole.autoSeconds ? 0.0 : 1.0;
    if (keyhole.command("publish")) {
      // BCI2000 definitions
      Serial.println("ResponseTone      1 0 0 0");
      Serial.println("ResponseToneMsec 15 0 0 0");
      Serial.println("Buzz              1 0 0 0");
      Serial.println("BuzzMsec         15 0 0 0");
      Serial.println();  // blank line to conclude definitions
    }
    keyhole.end();
  }
}
