class Trigger {
public:
  Trigger(int inputPin, int outputPin, int ledPin = -1, const String& name = "trigger")
    : mLastSampleTimeInMicros(0),
      mSamplePeriodInMicros(100),
      mInputPin(inputPin),
      mPreviousReading(0),
      mRecentMaxAbsDiff(0),
      mThreshold(200),
      mOutputPin(outputPin),
      mLEDPin(ledPin),
      mMinimumPulseDurationInMillis(10),
      mPulseDurationIncrementInMicros(10000),
      mRefractoryPeriodInMillis(200),
      mPulseOn(false),
      mInRefractoryPeriod(false),
      mTimeLastExceeded(0),
      mLastPulseStartTimeInMicros(0),
      mLastPulseFinishTimeInMicros(0),
      mLastPulseDurationInMicros(0),
      mPulsesStarted(0),
      mPulsesFinished(0),
      mSlidingWindow(1000, 20),
      mPulseStartCallback(NULL),
      mPulseFinishCallback(NULL),
      mName(name) {
    // Assuming mSamplePeriodInMicros is 100, each 1000-sample bin is 100ms.
    // Multiply that by 20 bins, and you get a total memory span of 2 seconds
    // in mSlidingWindow (although one bin always gets emptied when all bins
    // are full, so the memory span actually fluctuates between 1.9 and 2
    // seconds).

    if (mInputPin >= 0) pinMode(mInputPin, INPUT);
    if (mOutputPin >= 0) pinMode(mOutputPin, OUTPUT);
    if (mLEDPin >= 0) pinMode(mLEDPin, OUTPUT);
  }
  ~Trigger() {}

  unsigned long mLastSampleTimeInMicros;
  unsigned long mSamplePeriodInMicros;
  int mInputPin;
  int mPreviousReading;
  int mRecentMaxAbsDiff;
  int mThreshold;
  int mOutputPin;
  int mLEDPin;
  unsigned long mMinimumPulseDurationInMillis;
  unsigned long mPulseDurationIncrementInMicros;
  unsigned long mRefractoryPeriodInMillis;
  bool mPulseOn;
  bool mInRefractoryPeriod;
  unsigned long mTimeLastExceeded;
  unsigned long mLastPulseStartTimeInMicros;
  unsigned long mLastPulseFinishTimeInMicros;
  unsigned long mLastPulseDurationInMicros;
  unsigned long mPulsesStarted;
  unsigned long mPulsesFinished;
  RecentMax<int> mSlidingWindow;
  void (*mPulseStartCallback)(Trigger& t);
  void (*mPulseFinishCallback)(Trigger& t);
  String mName;

  virtual int getAbsDiff(void) {
    int currentReading = 0;
    if (mInputPin >= 0) currentReading = analogRead(mInputPin);
    int absDiff = abs(currentReading - mPreviousReading);
    mPreviousReading = currentReading;
    return absDiff;
  }

  bool process(unsigned long t) {
    if (t - mLastSampleTimeInMicros < mSamplePeriodInMicros) return mPulseOn;
    mLastSampleTimeInMicros = t;
    int absDiff = getAbsDiff();

    unsigned long microsSinceLastPulseStart = t - mLastPulseStartTimeInMicros;

    // If we think we're still in the refractory period, but the refractory duration has
    // in fact elapsed, mark the refractory period as finished:
    if (mInRefractoryPeriod && microsSinceLastPulseStart >= mRefractoryPeriodInMillis * 1000) mInRefractoryPeriod = false;

    bool exceeded = absDiff > mThreshold;
    if (exceeded) mTimeLastExceeded = t;

    if (mPulseOn) {
      if (microsSinceLastPulseStart >= mMinimumPulseDurationInMillis * 1000 && t - mTimeLastExceeded >= mPulseDurationIncrementInMicros) {  // The pulse always lasts 7--9 ms longer than the input both nominally (duration computed here) and objectively (when recorded with nidaq).
        // The extra duration depends on both the frequency content of the input, and the mPulseDurationIncrementInMicros (but this cannot be made much shorter than 10ms)
        if (mOutputPin >= 0) digitalWrite(mOutputPin, LOW);
        if (mLEDPin >= 0) digitalWrite(mLEDPin, LOW);
        mPulseOn = false;
        mPulsesFinished++;
        mLastPulseFinishTimeInMicros = t;
        mLastPulseDurationInMicros = microsSinceLastPulseStart;
        if (mPulseFinishCallback) mPulseFinishCallback(*this);
      }
    } else {
      if (!mInRefractoryPeriod && exceeded) {
        if (mOutputPin >= 0) digitalWrite(mOutputPin, HIGH);
        if (mLEDPin >= 0) digitalWrite(mLEDPin, HIGH);
        mPulseOn = true;
        mPulsesStarted++;
        mLastPulseStartTimeInMicros = t;
        if (mRefractoryPeriodInMillis > 0) mInRefractoryPeriod = true;
        if (mPulseStartCallback) mPulseStartCallback(*this);
      }
    }

    mRecentMaxAbsDiff = mSlidingWindow.process(absDiff);

    return mPulseOn;
  }
};

class MPU6050Trigger : public Trigger {
public:
  int mPreviousAccelX, mPreviousAccelY, mPreviousAccelZ;
  int mPreviousGyroX, mPreviousGyroY, mPreviousGyroZ;

  MPU6050Trigger(int outputPin, int ledPin = -1, const String& name = "trigger")
    : Trigger(-1, outputPin, ledPin, name),
      mPreviousAccelX(0), mPreviousAccelY(0), mPreviousAccelZ(0),
      mPreviousGyroX(0), mPreviousGyroY(0), mPreviousGyroZ(0) {
    mSamplePeriodInMicros = 1600;  // 625 Hz
    mSlidingWindow.resize(63, 20);
    // Each sample is 1600 micros, so each 63-sample bin is 100.8ms. Multiply that by 20 bins, and you
    // get a total memory span of 2.016 seconds (although one bin always gets emptied when all bins are
    // full, so the algorithm's memory span actually fluctuates between 1.9152 and 2.016 seconds)
  }
  ~MPU6050Trigger() {}
  void begin(void) {
    Wire.setClock(400000);  // max. data rate from the MPU6050 datasheet
    Wire.begin();
    // if we're not using the MPU6050 library, do the rest by hand using Wire instructions adapted from https://www.youtube.com/watch?v=yhz3bRQLvBY
    delay(250);                    // give the MPU6050 time to start up communication
    Wire.beginTransmission(0x68);  // 0x68 is the MPU6050's default I2C address
    Wire.write(0x6b);              // target the MPU6050's register at address 0x6B
    Wire.write(0x00);              // send 0 to that register to turn power on
    Wire.endTransmission();
  }
  virtual int getAbsDiff(void) {
    int absDiff = 0;

    Wire.beginTransmission(0x68);
    Wire.write(0x3b);  // send the address of the register where accel + gyro data begin
    Wire.endTransmission();
    Wire.requestFrom(0x68, 14);  // request 14 bytes starting at the aforementioned register
    int16_t currentAccelX = (Wire.read() << 8) | Wire.read();
    int16_t currentAccelY = (Wire.read() << 8) | Wire.read();
    int16_t currentAccelZ = (Wire.read() << 8) | Wire.read();
    Wire.read();
    Wire.read();  // discard the middle two bytes
    int16_t currentGyroX = (Wire.read() << 8) | Wire.read();
    int16_t currentGyroY = (Wire.read() << 8) | Wire.read();
    int16_t currentGyroZ = (Wire.read() << 8) | Wire.read();

#define GETDIFF(X) \
  absDiff += abs(current##X - mPrevious##X); \
  mPrevious##X = current##X;

    GETDIFF(AccelX);
    GETDIFF(AccelY);
    GETDIFF(AccelZ);
    GETDIFF(GyroX);
    GETDIFF(GyroY);
    GETDIFF(GyroZ);
    return absDiff;
  }
};
